﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {
            //construct an array and fill 'er up. Display on the screen.

            int[] id = new int[10];
            
            string[] courseName = new string[10];

            id[0] = 12345;
            id[1] = 56464;
            id[2] = 23158;
            id[3] = 98798;
            id[4] = 21684;
            id[5] = 68974;
            id[6] = 21564;
            id[7] = 38459;
            id[8] = 39785;
            id[9] = 68754;

            courseName[0] = "Potatoes";
            courseName[1] = "Carrots";
            courseName[2] = "Corn";
            courseName[3] = "Pumpkin";
            courseName[4] = "Beans";
            courseName[5] = "Peas";
            courseName[6] = "Broccoli";
            courseName[7] = "Cucumber";
            courseName[8] = "Turnips";
            courseName[9] = "Celery";

            Console.WriteLine("Original Array:");
            foreach (int i in id) {Console.WriteLine(i+ ""); }
            Console.WriteLine();
            foreach (string s in courseName) {Console.WriteLine(s + ""); }
            Console.WriteLine();
            Console.ReadKey();
            Console.Clear();            
            
            Program.arrayLength(id, courseName);    
            
            Program.arrayCopy(id, courseName);

            //Program.arrayType(id, courseName);

            Program.arrayValue(id, courseName);

            Program.arraySearch(id, courseName);

            Program.arrayReverse(id, courseName);

            Program.arrayChange(id, courseName);

            Program.arraySort(id, courseName);
        }

        static void arrayLength(int[] id, string[] courseName)
        {
            //get the length of both arrays:
            Console.WriteLine("ID Array Length:");
            Console.WriteLine(id.Length);
            Console.WriteLine("Course Name Length:");
            Console.WriteLine(courseName.Length);
            Console.WriteLine();
            Console.ReadKey();
            Console.Clear();
        }

         static void arrayCopy(int[] id, string[] courseName)
        {
            //copy each array
            int[] idCopy = new int[10];

            Array.Copy(id, idCopy, 10);

            string[] courseNameCopy = new string[10];

            Array.Copy(courseName, courseNameCopy, 10);

            Console.WriteLine("Coppied Array:");
            foreach (int i in idCopy) { Console.WriteLine(i + ""); }
            Console.WriteLine();
            foreach (string s in courseNameCopy) { Console.WriteLine(s + ""); }
            Console.WriteLine();
            Console.ReadKey();
            Console.Clear();
        }

        static void arrayType(int[] id, string[] courseName)
        {
            //Return the type for each array
            //Console.WriteLine($"Id type: {id.GetType}");
           // Console.WriteLine($"Course name Type: {courseName.GetType}");
            
        }

         static void arrayValue(int[] id, string[] courseName)
        {
            //get the value of each array at index 5
            Console.WriteLine("Values at index 5:");
            Console.Write($"{id[5]}");
            Console.Write(" ");
            Console.Write($"{courseName[5]}");
            Console.WriteLine();
            Console.ReadKey();
            Console.Clear();
        }

         static void arraySearch(int[] id, string[] courseName)
        {
            //search for a specific value in an array and display the output
            int input = 0;
            Console.WriteLine("Please type a number between 0 and 9 and press enter");
            input = int.Parse(Console.ReadLine());
            Console.WriteLine("The Items at that value are:");
            Console.Write($"{id[input]}");
            Console.Write(" ");
            Console.Write($"{courseName[input]}");
            Console.WriteLine();
            Console.ReadKey();
            Console.Clear();
        }

         static void arrayReverse(int[] id, string[] courseName)
        {
            //reverse the entire array
            Array.Reverse(courseName);
            Array.Reverse(id);
            Console.WriteLine("Reversed Array:");
            Console.WriteLine();
            foreach (int i in id) { Console.WriteLine(i + ""); }
            Console.WriteLine();
            foreach (string s in courseName) { Console.WriteLine(s + ""); }
            Console.WriteLine();
            Console.ReadKey();
            Console.Clear();
        }

         static void arrayChange(int[] id, string[] courseName)
        {
            //Change the values of both arrays at location 5
            Console.WriteLine("Please Type in a Course Name:");
            courseName[5] = Console.ReadLine();
            Console.WriteLine("Please type in a number for that course");
            id[5] = int.Parse(Console.ReadLine());

            Console.WriteLine("Values are now:");
            Console.Write($"{id[5]}");
            Console.Write(" ");
            Console.Write($"{courseName[5]}");
            Console.WriteLine();
            Console.ReadKey();
            Console.Clear();
        }

         static void arraySort(int[] id, string[] courseName)
        {
            //Sort the arrays in ascending and descending order
            Array.Sort(id);
            Array.Sort(courseName);
            Console.WriteLine();
            Console.WriteLine("Ascending Order:");
            Console.WriteLine();
            foreach (int i in id) { Console.WriteLine(i + ""); }
            Console.WriteLine();
            foreach (string s in courseName) { Console.WriteLine(s + ""); }
            Console.WriteLine();

            Array.Reverse(courseName);
            Array.Reverse(id);
            Console.WriteLine("Descending Order:");
            Console.WriteLine();
            foreach (int i in id) { Console.WriteLine(i + ""); }
            Console.WriteLine();
            foreach (string s in courseName) { Console.WriteLine(s + ""); }
            Console.WriteLine();

            Console.ReadKey();
            Console.Clear();
        }

         static void arrayType(int[] id)
        {
            
        }
    }
}
